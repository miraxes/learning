# ProCoders React Learning course

## Базовые видео курсы

[Дерево обучения](https://edu.cbsystematics.com/Images/RoadMap/Roadmap_Frontend-min.jpg)


### 1. Javascript
* [Learn JS for dummies](http://www.learn-js.org/)
* разобраться в деталях ES2015 на основе [презентации](http://courseware.codeschool.com.s3.amazonaws.com/es2015-the-shape-of-javascript-to-come/all-levels.pdf) знать досконально!


### 2. Видео-курсы React

Слушать видно курсы можно на скорости х1.25 и даже разогнаться до х1.75. Используйте [VLC player](https://www.videolan.org/index.ru.html)
* Если никогда не работал с React, тогда [Краткий курс React новичок от javascript.ru](https://learn.javascript.ru/screencast/react)
* Полный курс по реакт на 35 часов [Javascript.Ninja | Базовый React (2018)](https://nnm-club.me/forum/viewtopic.php?t=1233140)
* Потом, на фоне можно послшать курс-webinar тоже на 35 часов [React.js. Разработка веб-приложений](https://nnm-club.me/forum/viewtopic.php?t=1233144)
* Получить понимание как можно делать, а как не нужно [Курс-тренинг базового уровня](magnet:?xt=urn:btih:399064919B08F5D57369430739E4F2D2B051CA74&dn=%d0%9a%d1%83%d1%80%d1%81%20%d0%bf%d0%be%20React.js&tr=http%3a%2f%2fbt01.nnm-club.cc%3a2710%2fffffffffff8c3902ba88fc68c3bbfae2%2fannounce&tr=http%3a%2f%2fbt01.nnm-club.info%3a2710%2fffffffffff8c3902ba88fc68c3bbfae2%2fannounce&tr=http%3a%2f%2fretracker.local%2fannounce.php%3fsize%3d5168573871%26comment%3dhttp%253A%252F%252Fnnm-club.name%252Fforum%252Fviewtopic.php%253Fp%253D9153299%26name%3d%25D0%25EE%25EC%25E0%25ED%2b%25DF%25EA%25EE%25E1%25F7%25F3%25EA%2b%257C%2b%25CA%25F3%25F0%25F1%2b%25EF%25EE%2bReact.js%2b%25282017%2529%2bPCRec&tr=http%3a%2f%2f%5b2001%3a470%3a25%3a482%3a%3a2%5d%3a2710%2fffffffffff8c3902ba88fc68c3bbfae2%2fannounce&ws=)

Видео можно смотреть на скорости 1.5х , удобно через 

### 3. Пишем на Typescript
* разобраться с [TypeScript за 5 минут](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)
и с [Typescript OOP](https://www.typescriptlang.org/docs/handbook/basic-types.html)
* Гайд как писать на [Typescript in React](https://medium.com/@abraztsov/%D0%B3%D0%B0%D0%B9%D0%B4-%D0%BA%D0%B0%D0%BA-%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D1%8C-%D0%BD%D0%B0-react-%D0%B2-2017-8128906dae80)

### 4. React base patterns
* [LifeCycle](https://levelup.gitconnected.com/componentdidmakesense-react-lifecycle-explanation-393dcb19e459)
* [React Router v4](https://habrahabr.ru/post/329996/)
* [Decorator, HOC](https://medium.freecodecamp.org/evolving-patterns-in-react-116140e5fe8f)
* [componentDidCatch](https://medium.com/@blairanderson/react-v16-new-error-handler-example-d62499117bfa)
* [React Context API](https://itnext.io/understanding-the-react-context-api-through-building-a-shared-snackbar-for-in-app-notifications-6c199446b80c)
* [8 methods of redering REACT](https://blog.logrocket.com/conditional-rendering-in-react-c6b0e5af381e)
* [8 no-Flux strategies for React component communication](https://www.javascriptstuff.com/component-communication/)

### 5. Разобраться с mobx
* [MobX tutorial #1](https://www.youtube.com/watch?v=_q50BXqkAfI)
* [Mobx и управление состоянием](https://habr.com/company/yandex/blog/339054/)
* [MobX и асинхронность](https://habr.com/company/qiwi/blog/340840/)
* [MobX React best practice](https://medium.com/dailyjs/mobx-react-best-practices-17e01cec4140)
* [MobX utils](https://github.com/mobxjs/mobx-utils)

### 6. тестирование в react
* [JEST](https://semaphoreci.com/community/tutorials/how-to-test-react-and-mobx-with-jest)

### 7. Node.JS

### 8. Утилиты и библиотеки
* [Json-Server](https://medium.com/codingthesmartway-com-blog/create-a-rest-api-with-json-server-36da8680136d) для быстрого мока бекенда 
* [Мышление в стиле Ramda: Первые шаги](https://habr.com/post/348868/)

### 9. GraphQL
* [Что такое GraphQL](https://habr.com/post/326986/)
* [Переход от REST API к GraphQL](https://habr.com/post/334182/)
* [Complete video Tutorial for GraphQL](https://www.howtographql.com/)
* [React, Apollo, GraphQL. Пример блога за 5 минут на русском](https://habr.com/post/358292/)
* [GraphQL, Apollo. Что же упрощается ](https://habr.com/post/331088/)
* [Complete React Apollo Tutorial](https://www.robinwieruch.de/react-graphql-apollo-tutorial/)
* [Apollo and SSR](https://habr.com/post/358942/)

### 9. SSR Next.js
* [SSR доклад](https://www.youtube.com/watch?v=uB7Yz4nH8nc)
* [Next.js official](https://nextjs.org/learn/)
* [Урок работы с Next.js](https://webformyself.com/urok-po-next-js-seo-friendly-react-e-commerce-spa/)


### 10. попробовать ReactNative
* [Official tutorial](https://facebook.github.io/react-native/docs/tutorial.html)
* Собрание [Tutorials React Native](https://proglib.io/p/react-native-tutorials/)
* [Video React Native - The Practical Guide](https://coursehunters.net/course/react-native-prakticheskoe-rukovodstvo)


## Общие скиллы

### 1. VsCode
* [Tips and Tricks for VSCode](https://github.com/Microsoft/vscode-tips-and-tricks)

### 2. Принципы программирования
* [Паттерны](https://www.youtube.com/watch?v=Z90DFL2Ndow)
* [SOLID принципы](https://www.youtube.com/watch?v=59tq5Fcgn7A)